from bisect import bisect

import Colors as color
import CustomButtons as button
import CustomSlider as slider
import pyqtgraph as pg
import Config
from Config import config
from PyQt5 import QtCore, QtWidgets, QtNetwork, QtGui
from PyQt5.QtGui import QColor
from net import Networking


class SamplerUI(pg.GraphicsWindow):
    def __init__(self, receive_port, send_port):
        super().__init__(parent=None)
        # Set up networking
        self.udp_socket = QtNetwork.QUdpSocket(self)
        self.udp_socket.bind(QtNetwork.QHostAddress(''), receive_port)
        self.send_port = send_port

        # Set up the main layout
        self.mainLayout = self.controlsLayout = self.plotDataItem = self.record_button = None
        self.init_layout()

        # Variables for record
        self.bars = dict()
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.receive_packets)
        self.timer.start()

        # variables for slices
        self.slices = []
        self.slider_step = 0.1
        self.previous_threshold = None

        # Constants
        self.sample_rate = 25

    def init_layout(self):
        # Add main layout
        self.mainLayout = QtWidgets.QVBoxLayout()
        self.controlsLayout = QtWidgets.QHBoxLayout()
        self.setLayout(self.mainLayout)
        self.mainLayout.addStretch(config["topControlSpacerWeight"])
        self.mainLayout.addLayout(self.controlsLayout, config["controlPanelWeight"])

        # Add button(s)
        recordParams = Config.getRecordButtonParams()
        self.record_button = button.CircleCircleButton(self, recordParams, self.record)
        self.record_button.setActive()
        self.controlsLayout.addStretch(config["controlPanelLeftSpacerWeight"])
        self.add_widget(self.controlsLayout, self.record_button, config["controlPanelRecordButtonWeight"])

        # Add slider
        self.controlsLayout.addStretch(config["controlPanelCenterSpacerWeight"])
        sliderParams = Config.getSliderParams()
        self.add_widget(self.controlsLayout, slider.HorizontalSlider(self, sliderParams, self.slider_value), config["controlPanelSliderWeight"])
        self.controlsLayout.addStretch(config["controlPanelRightSpacerWeight"])

        # Add graph
        self.plotDataItem = Plot()
        self.plotDataItem.setMenuEnabled(False)
        self.mainLayout.addStretch(config["waveformControlSpacerWeight"])
        self.add_widget(self.mainLayout, self.plotDataItem, config["waveformPanelWeight"])
        self.mainLayout.addStretch(config["bottomWaveformSpacerWeight"])

    def send(self, data):
        try:
            tcp_send = Networking("tcp", True, backend_ip='localhost', port=self.send_port)
        except Exception as error:
            print("Waiting for Ableton...")
            print("Got the following error: {}".format(error))
            self.record_button.setActive()
        else:
            tcp_send.send(data)
            tcp_send.close_connection()

    @staticmethod
    def add_widget(layout, widget, pos):
        layout.addWidget(widget, pos)

    def receive_packets(self):
        """Receives messages from back end, and redirect messages to correct method"""
        while self.udp_socket.hasPendingDatagrams():
            data = self.udp_socket.receiveDatagram().data().data().decode().split()
            if data[0] == '20':
                self.add_bar(int(float(data[2]) / self.sample_rate), float(data[1]))
            elif data[0] == '30':
                if -int(-float(data[2]) // self.sample_rate) in self.bars.keys():
                    self.add_slices(data)
            elif data[0] == '50':
                self.init_record()
            elif data[0] == '1':
                self.record_button.setActive()

    def record(self):
        """Starts recording"""
        self.init_record()
        self.record_button.setInactive()
        self.send("0")

    def init_record(self):
        """Clears the variables needed for recording and drawing."""
        self.init_slice()
        self.bars.clear()
        self.plotDataItem.clear()

    def add_bar(self, x, height):
        """Draws the sound wave."""
        if x in self.bars.keys():
            return
        bar = BarGraph(self, [x] * 2, [-height, height])
        self.plotDataItem.addItem(bar)
        self.bars[x] = bar

    def slider_value(self, value):
        """Takes in the values from the slider"""
        new_v = round(round((value * 2) / self.slider_step) * self.slider_step, len(str(self.slider_step)))
        if new_v != self.previous_threshold:
            self.slice(new_v)
            self.previous_threshold = new_v

    def add_slices(self, data):
        """Adds slices to plot based on the input"""
        bar = self.bars[-int(-float(data[2]) // self.sample_rate)]
        bar.index = data[1]
        if data[1] != '1':
            self.slices.append(bar)
            self.clear_bars()
            self.set_slices(self.slices)

    def init_slice(self):
        """Prepares for slicing"""
        self.remove_slices(self.slices)
        self.slices.clear()

    def slice(self, threshold):
        """Slices the plot"""
        self.init_slice()
        self.send("31 " + str(threshold))

    def clear_bars(self):
        """Clear all bars"""
        for i in self.bars.values():
            i.change_to_release()

    @staticmethod
    def remove_slices(slices):
        """Removes the slices from the plot
        :param slices: A list containing slices to be removed
        """
        for i in slices:
            i.index = None
            i.change_to_release()

    @staticmethod
    def set_slices(slices):
        """Draws the slices on the plot
        :param slices: A list containing slices to be drawn
        """
        for i in slices:
            i.set_slice()

    def slice_event(self, x, event):
        """Highlights the slices pressed
        :param x: The bar pressed
        :param event: 0 if it was a press, or 0 if it was a release
        """
        slices = sorted(list(set([-1] + [x.opts['x'][0] for x in self.slices] + [max(self.bars.keys()) + 1])))
        index = bisect(slices, x)
        lower = slices[index - 1]
        upper = slices[index]

        if event == 0:
            self.send("10 {} 127".format(self.bars[lower if lower != -1 else 0].index))
            for i in [self.bars[key] for key in sorted(self.bars.keys())][lower + 1:upper]:
                i.change_to_press()
        else:
            self.send("10 {} 0".format(self.bars[lower if lower != -1 else 0].index))
            for i in [self.bars[key] for key in sorted(self.bars.keys())][lower + 1:upper]:
                i.change_to_release()


class Plot(pg.PlotWidget):
    """The plot class where the waveform is drawn"""

    def __init__(self):
        super().__init__()
        self.setMouseEnabled(False, False)
        self.setXRange(0, 200)
        self.setYRange(-1, 1)
        self.hideAxis("bottom")
        self.hideAxis("left")


class BarGraph(pg.BarGraphItem):
    """The bar class representing sounds"""

    def __init__(self, layout, x, height):
        self.layout = layout
        self.index = None
        self.color = config["waveformColor"]
        self.press_color = config["waveformColor_active"]
        self.slice_color = config["sliceColor"]
        self.original_height = height
        super().__init__(x=x, height=height, pen=self.color, brush=self.color, width=0.6)

    def mousePressEvent(self, ev):
        """Decides what to do when a bar is clicked."""
        self.layout.slice_event(self.opts['x'][0], 0)

    def mouseReleaseEvent(self, ev):
        """Decides what to do when a bar is released."""
        self.layout.slice_event(self.opts['x'][0], 1)

    def change_to_press(self):
        self.setOpts(pen=self.press_color, brush=self.press_color)

    def set_slice(self):
        self.setOpts(pen=self.slice_color, brush=self.slice_color, height=[-1, 1])

    def change_to_release(self):
        self.setOpts(pen=self.color, brush=self.color, height=self.original_height)


if __name__ == '__main__':
    Config.loadConfig()
    app = QtWidgets.QApplication([])
    pg.setConfigOptions(antialias=False)
    win = SamplerUI(4444, 4445)
    win.showFullScreen()
    win.raise_()
    app.exec_()
