import socket


class Networking:
    def __init__(self, protocol, send, sock=None, backend_ip='192.168.43.250', port=4444, buffer_size=1024):
        """
        :param protocol: Takes in either 'udp' for UDP or 'tcp for TCP.
        :param send: True if the object is going to send, or False if it is a receiver. Does not matter for TCP.
        :param sock: Takes in existing socket.
        :param backend_ip: IP address for backend.
        :param port: Port number for backend.
        :param buffer_size: Size of the receiving and sending buffer in bytes.
        """
        self.server = backend_ip
        self.port = port
        self.buffer_size = buffer_size
        self.protocol = protocol
        self.sock = socket.socket(socket.AF_INET, {"udp": socket.SOCK_DGRAM,
                                                   "tcp": socket.SOCK_STREAM}[self.protocol]) if sock is None else sock

        if send is False or protocol != "udp":
            self.connect()

    def connect(self):
        """
        Connects to the socket
        :param host: IP address for socket
        :param port: Port number for socket
        """
        {"udp": self.sock.bind, "tcp": self.sock.connect}[self.protocol]((self.server, self.port))

    def recv(self):
        """
        Receives message from socket
        :return: Message as a string
        """
        data = b''
        self.sock.settimeout(5)
        while True:
            if self.protocol == "tcp":
                part = self.sock.recv(self.buffer_size)
            else:
                part, addr = self.sock.recvfrom(self.buffer_size)
            data += part
            if len(part) < self.buffer_size:
                break
        return data.decode()

    def send(self, message):
        """
        Messages to be sent to the socket
        :param message: A string to be sent
        """
        total_sent = 0
        message = message.encode()

        while total_sent < len(message):
            sent = self.sock.send(message[total_sent:]) if self.protocol == "tcp" else \
                self.sock.sendto(message[total_sent:], (self.server, self.port))
            total_sent += sent

    def close_connection(self):
        """
        Needs to be called if it is a TCP connection or a receiving UDP connection.
        """
        self.sock.close()
