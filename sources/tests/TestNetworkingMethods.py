import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

import unittest
import socket
import threading
from net import Networking
import time


class TestNetworkingMethods(unittest.TestCase):
    def setUp(self):
        self.port = 4444
        self.msg = None

    def sending_tcp_server(self):
        server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_sock.bind((socket.gethostname(), self.port))
        server_sock.listen(5)
        c, adr = server_sock.accept()
        c.send(b'hello')
        server_sock.close()

    def sending_udp_server(self):
        server_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        time.sleep(1)
        server_sock.sendto(b'hello', (socket.gethostname(), self.port))

    def receiving_tcp_server(self):
        self.msg = None
        server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_sock.bind((socket.gethostname(), self.port))
        server_sock.listen(5)
        c, adr = server_sock.accept()
        self.msg = c.recv(1024)
        server_sock.close()

    def receiving_udp_server(self):
        self.msg = None
        server_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server_sock.bind((socket.gethostname(), self.port))
        self.msg, addr = server_sock.recvfrom(1024)
        server_sock.close()

    def test_receive_tcp(self):
        # Start fake server in background thread
        server_thread = threading.Thread(target=self.sending_tcp_server)
        server_thread.start()

        time.sleep(1)

        # Test the client receiving from server
        client = Networking("tcp", False, backend_ip=socket.gethostname(), port=self.port)
        msg = client.recv()
        self.assertEqual('hello', msg)
        client.close_connection()

        # Ensure server thread ends
        server_thread.join()

    def test_send_tcp(self):
        # Start fake server in background thread
        server_thread = threading.Thread(target=self.receiving_tcp_server)
        server_thread.start()

        time.sleep(1)

        # Test the client sending to server
        client = Networking("tcp", True, backend_ip=socket.gethostname(), port=self.port)
        client.send('hello')
        time.sleep(1)
        self.assertEqual(b'hello', self.msg)
        client.close_connection()

        # Ensure server thread ends
        server_thread.join()

    def test_receive_udp(self):
        # Start fake server in background thread
        server_thread = threading.Thread(target=self.sending_udp_server)
        server_thread.start()

        # Test the client receiving from server
        client = Networking("udp", False, backend_ip=socket.gethostname(), port=self.port)
        msg = client.recv()
        self.assertEqual('hello', msg)
        client.close_connection()

        # Ensure server thread ends
        server_thread.join()

    def test_sending_udp(self):
        # Start fake server in background thread
        server_thread = threading.Thread(target=self.receiving_udp_server)
        server_thread.start()

        # Test the client sending to server
        client = Networking("udp", True, backend_ip=socket.gethostname(), port=self.port)
        time.sleep(1)
        client.send('hello')
        time.sleep(1)
        self.assertEqual(b'hello', self.msg)

        # Ensure server thread ends
        server_thread.join()


if __name__ == '__main__':
    unittest.main()
