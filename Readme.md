# Alpaca Sampler 

This is a simplified sampler interface for Ableton. It is meant to be connected over network with an Ableton session.

# How to contribute

## Sources organization 

The files are organized in a never-looking-up tree : An implementation file shall never access directly a file that is not in the same directory or one of its directory's sons. If a file looks up (explores it's parent), it only is allowed to look for abstraction files (interfaces and abstract classes), and never to go down in one of its brothers.

Example : A : [B:[c, d, E], F:[G:[h], i, j], k, m]. A is the root directory containing B, F, and two files :k (abstraction) and m (implementation). B contains files c, d, and directory E. F contains files i, j and directory G, itself containing the file h. The i implementation file is allowed to access freely j, h and k, and is forbiden to access m and any file in B.

The idea here is to provide strong code separation. Tests corresponding a file has to be either in the same directory, or in a directory that is the direct son of the current file directory. 

This division should allow and encourage a strong decoupling in the code.

## Branches and commits

### Master and Dev branches 
-- Master is the always-functioning product branch. No pushes are allowed on it, and the only merges are done by mainteners and only with Dev branch. This ensures that a functional and clean version of the software is available at any time.

-- Dev is the always-functioning development branch. No pushes are allowed on it, but it can be merged by anyone. Only functional merges are allowed on this branch. This branch is used to merge other branches (see below) and thus the work of developers. The responsibility of having the code working on the branch is set on the merging person, but any person whose code is involved in the merge has the duty to help him getting it functional if needed. Merging with this branch is only relevant when a new functionality or step is achieved.

### Other branches 

Each person is free to decide how to manage his own branching, as long as the Dev and Master requirements are needed. 
Branching can be done either one per developer, or one per functionality or step, or a combination of the two. 

### Commits 

Commits shall be done wisely. Too many commits can make the commit flow unclear, and encourage "dirty commits", while not enough commits make each commit heavier to read and understand, and may not be sufficiently refined for versioning and debugging. The advise is to commit each time a major change comes, and to only commit the files of the major change. If two major changes occurs at the same time, try to branch or be careful to be as refined as possible when including files in the commit. 

Commit messages shall be clear and precise of what the commit is used for. The head of the message (first line) shall be short and describe shortly but precisely what the commit is about : this line will be displayed in the gitlab repos. The body of the message (other lines) has to describe precisely : What is the commit solving/bringing, what changes it uses to do it, why it uses this changes. This description is particularly important when the commit touches several files, or files out of the scope of the developer.  Always be straightforward when writing a commit message.

Some interesting commands :
git add -A
git add *filepath* [*filepath*, ...]
git add -u 

## Coding

### Coding style

There is not mandatory coding style, do as you please, as long as you are coherent in your code and respect the coding style of already existing files when editing them.

### Code practices

-- Try to make at maximum the code self explanatory, but don't forget that comments are awesome when used wisely. If something can be unclear in the code, please document it.

-- Clear your code regularly and do not hesitate to refactor it if needed. Peer review is also the best option to validate code. 

-- Tell other developers if their code is unclear or you think it can be improved when you use it. 

### No ***** global variable 

Do never use global variables or global-scope code. Any entity (class, function, ...) of the code that is not scoped (under another class or function or ...) has to be instanciable more than one time at once. The only exception is for shared resource managers.

## Conception

Under the conception directory are the conception files, as : UML, user interface drawings, ... As this a git repository, please prefer text-based file formats (ex : PlantUML for UML), so the versioning system can handle them easily. 
